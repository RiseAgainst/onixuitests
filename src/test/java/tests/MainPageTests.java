package tests;

import base.Base;
import org.testng.annotations.Test;
import pages.*;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;


@Features("Public site main page tests")
public class MainPageTests extends Base{

    @Test
    @Stories("Check scroll is working and elements are loaded")
    public void testCanScrollMainPage(){
        new MainPage().checkScrollElementsAreLoaded();
    }

    @Test
    @Stories("Check contact us button on the main page is working")
    public void testMainPageContactUsButton(){
        MainPage mainPage = new MainPage();
        mainPage.scrollScreenToTheFooter();
        ContactsPage contactUs = mainPage.clickContactUsButton();
        contactUs.checkPageIsLoaded();
    }

    @Test
    @Stories("Check title and footer information")
    public void testCheckStaticDataOnTheMainPage(){
        MainPage mainPage = new MainPage();
        mainPage.scrollScreenToTheFooter()
                .checkTitle()
                .checkFooterStaticInformation();
    }
}
