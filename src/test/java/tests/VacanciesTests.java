package tests;

import base.Base;
import org.testng.annotations.Test;
import pages.ContactsPage;
import pages.MainPage;
import pages.VacanciesPage;
import pages.vacancies.Vacancy;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import java.util.List;



@Features("Vacancies page tests")
public class VacanciesTests extends Base {

    @Test
    @Stories("Check page and all elements are loaded")
    public void testCheckPageIsLoaded(){
        new MainPage().clickVacanciesLink().checkPageIsLoaded();
    }

    @Test
    @Stories("Check vacancy details text and values")
    public void checkVacancyDetails() {
        VacanciesPage vacanciesPage = new VacanciesPage();
        Vacancy vacancy = vacanciesPage.clickSeniorIOSDeveloper();
        vacancy.checkTitle();
        vacancy.checkVacancyText();
        vacancy.checkVacancyDropdownDefaultValue();
    }

    @Test
    @Stories("Check vacancy value by default")
    public void checkVacancyDefaultValue() {
        VacanciesPage vacanciesPage = new VacanciesPage();
        Vacancy vacancy = vacanciesPage.clickSeniorPhpDeveloper();
        vacancy.checkVacancyDropdownDefaultValue();
    }

    @Test
    @Stories("User able to select any available vacancy")
    public void testCheckVacancyDropdownValues(){
        List<String> vacanciesList = new VacanciesPage().getVacanciesListFromVacanciesPage();
        ContactsPage contactUs = new ContactsPage();
        contactUs.checkVacancyDropdownValues(vacanciesList);
    }

}
