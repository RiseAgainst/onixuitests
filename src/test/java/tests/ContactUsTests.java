package tests;

import base.Base;
import data.Aspirants;
import data.Vacancy;
import data_provider.DataProviders;
import org.testng.annotations.Test;
import pages.ContactsPage;
import pages.MainPage;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

@Features("Contact us page tests")
public class ContactUsTests extends Base {

    @Test
    @Stories("Check page and all elements are loaded")
    public void testCheckPageIsLoaded(){
        new MainPage().clickContactUsLink().checkPageIsLoaded();
    }

    @Test
    @Stories("Check user can send message with contact us form")
    public void testCanSendMessageWithValidData() {
        ContactsPage contactUs = new ContactsPage();
        contactUs.fillContactFormFields(Aspirants.validAspirant);
        contactUs.uploadDefaultCv();
        contactUs.selectVacancy(Vacancy.seniorJavaScript);
      //  contactUs.clickSendButton();
    }

    @Test(dataProvider = "invalidEmails", dataProviderClass = DataProviders.class)
    @Stories("Check validation for email field")
    public void testCheckEmailFieldValidation(String email){
        ContactsPage contactUs = new ContactsPage();
        contactUs.fillEmailField(email);
        contactUs.clickSendButton();
        contactUs.checkErrorMessageIncorrectEmail();
    }

    @Test
    @Stories("Check validation for required fields")
    public void testCheckRequiredFields() {
        ContactsPage contactUs = new ContactsPage();
        contactUs.clickSendButton();
        contactUs.checkErrorMessagesRequiredFields();
    }
}
