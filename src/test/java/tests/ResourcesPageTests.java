package tests;

import base.Base;
import org.testng.annotations.Test;
import pages.MainPage;
import pages.ResourcesPage;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

@Features("Resource page tests")
public class ResourcesPageTests extends Base {

    @Test
    @Stories("Check that navigation menu is working")
    public void testCanUseNavigationMenu(){
        ResourcesPage resources = new MainPage().clickResourcesLink();
        resources.clickOnTheCategory("Ruby");
        resources.checkContentIsChanged();
    }

}
