package data_provider;

import org.testng.annotations.DataProvider;

public class DataProviders {

    @DataProvider
    public static Object[][] invalidEmails() {
        return new Object[][]{
                {"plainaddress"},
                {"user@domain"},
                {"#@%^%#$@#$@#.com"},
                {"@domain.com"},
                {"email.domain.com"}
        };
    }
}

