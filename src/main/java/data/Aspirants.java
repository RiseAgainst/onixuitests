package data;


import models.Aspirant;

public class Aspirants {
    public static Aspirant validAspirant = new Aspirant("Bill", "Gates", "bill@ukr.net", "0981234567", "Hello. This is automation test-bot");
    public static Aspirant invalidAspirant = new Aspirant(" 123- 65= kjfh ", " jhf 454 455 (((())- = ", " fddfgdfd435345", "q", "                        ");
}
