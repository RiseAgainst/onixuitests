package data;


public class Vacancy {

    public static String webDesigner = "Web (UI/UX) дизайнер для створення сайтів та мобільних додатків";
    public static String projectManager = "Project Manager";
    public static String salesExecutive = "Sales Executive";
    public static String unityEngineer = "Unity Software Engineer";
    public static String seniorIos = "Senior iOS developer";
    public static String seniorPhp = "Senior PHP developer";
    public static String devOps = "Middle/Senior DevOps";
    public static String seniorAndroid = "Senior Android developer";
    public static String midleJavaScript = "Middle JavaScript developer";
    public static String seniorJavaScript = "Senior JavaScript developer";
    public static String qa = "QA";
}
