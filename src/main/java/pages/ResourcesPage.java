package pages;


import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import org.testng.Assert;
import utils.Misc;
import static com.codeborne.selenide.Selenide.$;


public class ResourcesPage {

    private static String menuItemValue;

    public ResourcesPage() {
        Selenide.open("resources");
    }

    public void clickOnTheCategory(String categoryValue) {
        this.menuItemValue = categoryValue;
        $(By.xpath("//a[contains(text(), '" + categoryValue + "')]")).click();
    }

    public void checkContentIsChanged() {
        SelenideElement content = $(By.xpath("//*[@class = 'main-title' and contains(text(), '" + this.menuItemValue + "')]"));
        Misc.waitFor(1000);
        Boolean visibilityStatus = Misc.isVisibleInViewport(content);
        Assert.assertTrue(visibilityStatus, "Element should be visible");
    }
}
