package pages.vacancies;

import com.codeborne.selenide.Selenide;
import pages.vacancies.Vacancy;

public class MiddleSeniorDevOpsPage extends Vacancy {

    String title = "Middle/Senior DevOps – Onix-Systems";

    public MiddleSeniorDevOpsPage(){
        this.title = title;
    }
}
