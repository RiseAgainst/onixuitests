package pages.vacancies;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import org.testng.Assert;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.title;

public class Vacancy {
    private SelenideElement defaultValueToDropDown = $(By.xpath("//*[@class='CSP-item-value']"));
    private SelenideElement responsibilitiesBlock =  $(By.xpath("//*[contains(text(), 'Обов')]/.."));
    private SelenideElement requiredSkillsBlock = $(By.xpath("//*[text() = 'Основні вимоги:']/.."));
    protected String expectedTitle;
    protected String expectedDropdownVacancyValue;
    protected String expectedResponsibilitiesValue;
    protected String expectedRequiredSkills;


    public void checkTitle() {
        Assert.assertTrue(title().equals(expectedTitle), "Wrong page title. Expected:" + expectedTitle + " Actual:" + title());
    }

    public void checkVacancyDropdownDefaultValue() {
        defaultValueToDropDown.shouldHave(Condition.text(expectedDropdownVacancyValue));
    }

    public void checkResponsibilitiesValue() {
        responsibilitiesBlock.shouldHave(Condition.text(expectedResponsibilitiesValue));
    }

    public void checkExpectedRequiredSkills() {
        requiredSkillsBlock.shouldHave(Condition.text(expectedRequiredSkills));
    }

    public void checkVacancyText() {
        checkResponsibilitiesValue();
        checkExpectedRequiredSkills();
    }

}
