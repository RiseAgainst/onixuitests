package pages.vacancies;


public class SeniorAndroidDeveloperPage extends Vacancy {

    String title = "Senior Android developer – Onix-Systems";

    public SeniorAndroidDeveloperPage(){
        this.expectedTitle = title;
    }
}
