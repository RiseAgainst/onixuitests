package pages.vacancies;



public class SeniorJavaScriptDeveloperPage extends Vacancy{

    String title = "Senior JavaScript developer – Onix-Systems";

    public SeniorJavaScriptDeveloperPage(){
        this.expectedTitle = title;
    }
}
