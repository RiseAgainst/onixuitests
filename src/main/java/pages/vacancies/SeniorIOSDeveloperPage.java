package pages.vacancies;



public class SeniorIOSDeveloperPage extends Vacancy {

    public SeniorIOSDeveloperPage(){
        this.expectedTitle = "Senior iOS developer – Onix-Systems";
        this.expectedDropdownVacancyValue = data.Vacancy.seniorIos;
        this.expectedResponsibilitiesValue = "Обов’язки:\n" +
                "Робота з новими шаблонами проектування, вміння використовувати шаблони під час написання коду.\n" +
                "Використання класичних алгоритмів та структур даних.\n" +
                "Аналіз умов та вибір оптимальних способів збереження та обробки даних.\n" +
                "Робота з базами даних Sqlite та Realm.";
        this.expectedRequiredSkills = "Основні вимоги:\n" +
                "Досвід розробки iOS додатків від 3-х років.\n" +
                "Впевнені знання Objective-C та Swift.\n" +
                "Поглиблені знання мережевих протоколів та роботи з сокетами.\n" +
                "Знання iOS SDK та основних фреймворків (Foundation, UIKit, CoreData, AutoLayout, CoreGraphics, MapKit), форматів XML та JSON.\n" +
                "Досвід роботи з Push Notifications та розуміння їх обмежень в роботі.\n" +
                "Досвід роботи з SOAP та REST-сервісами.\n" +
                "Знання особливостей та відмінностей під iPad, iPhone, iPod.\n" +
                "Досвід профілювання додатків, пошук вузьких місць та оптимізація.\n" +
                "Знання роботи з бінарними форматами файлів, оптимізація по швидкості та використанні пам’яті.\n" +
                "Вміння працювати з пристроями на низькому рівні, iBeacon та BLE.\n" +
                "База знань роботи з OpenGL и Metal.\n" +
                "Бажані знання libPng, libJpeg та Open Source, а також досвід портування Open Source бібліотек.\n" +
                "Знання C++ з використанням STL є перевагою.\n" +
                "Володіння англійською мовою.";
    }
}
