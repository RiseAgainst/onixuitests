package pages.vacancies;

import com.codeborne.selenide.Selenide;
import pages.vacancies.Vacancy;

public class QAPage extends Vacancy {

    String title = "QA – Onix-Systems";

    public QAPage(){
        this.expectedTitle = title;
    }
}
