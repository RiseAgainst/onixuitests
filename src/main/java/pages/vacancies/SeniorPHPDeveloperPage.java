package pages.vacancies;


public class SeniorPHPDeveloperPage extends Vacancy{

    String title = "Senior PHP developer – Onix-Systems";

    public SeniorPHPDeveloperPage(){
        this.expectedTitle = title;
        this.expectedDropdownVacancyValue = "Not valid value";
    }
}
