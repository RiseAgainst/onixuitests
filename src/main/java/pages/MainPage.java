package pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import org.testng.Assert;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.title;

public class MainPage {
    private SelenideElement aboutUs = $(By.xpath("//*[@href='https://onix.kr.ua/' and text()='Про нас']"));
    private SelenideElement vacanciesLink = $(By.xpath("//*[@href='https://onix.kr.ua/vacancies/']"));
    private SelenideElement contactUsLink = $(By.xpath("//*[@href='https://onix.kr.ua/contacts/']"));
    private SelenideElement resourcesLink = $(By.xpath("//*[@href='https://onix.kr.ua/resources/']"));
    private SelenideElement contactWithUsButton  = $(By.xpath("//*[@class='callback-button animated fadeInDown']"));
    private By photos = By.xpath("//ul[@class = 'photos animated fadeIn']/li");
    private By advantages = By.xpath("//ul[@class = 'advantages']/li");
    private By values = By.className("list-content");
    private By testimonials = By.className("easy_testimonial");
    private SelenideElement facebookLink = $(By.className("facebook"));
    private SelenideElement twitterLink = $(By.className("twitter"));
    private SelenideElement linkedinLink = $(By.className("linkedin"));
    private SelenideElement footerEmail = $(By.xpath("//*[contains(text(), 'Email')]"));
    // Expected data
    private static String expectedTitle = "Onix-Systems – Створюй майбутнє разом з нами";
    private static String expectedFacebookLink = "https://www.facebook.com/onix.kr.ua/";
    private static String expectedTwitterLink = "https://twitter.com/onix_systems";
    private static String expectedLinkedInLink = "https://www.linkedin.com/company/onix-systems";
    private static String expectedEmail = "hr@onix-systems.com";

    public MainPage(){ Selenide.open("/");}

    public MainPage checkTitle() {
        Assert.assertTrue(title().equals(expectedTitle), "Wrong page title");
        return this;
    }

    public void checkFooterStaticInformation() {
        facebookLink.shouldHave(Condition.attribute("href",expectedFacebookLink ));
        twitterLink.shouldHave(Condition.attribute("href",expectedTwitterLink ));
        linkedinLink.shouldHave(Condition.attribute("href",expectedLinkedInLink ));
        footerEmail.shouldHave(Condition.text(expectedEmail));
    }

    public MainPage clickAboutUsLink(){
        aboutUs.click();
        return this;
    }

    public ContactsPage clickContactUsLink(){
        contactUsLink.click();
        return new ContactsPage();
    }

    public VacanciesPage clickVacanciesLink(){
        vacanciesLink.click();
        return new VacanciesPage();
    }

    public ResourcesPage clickResourcesLink(){
        resourcesLink.click();
        return new ResourcesPage();
    }

    public ContactsPage clickContactUsButton(){
        contactWithUsButton.click();
        return new ContactsPage();
    }

    public MainPage scrollScreenToTheFooter(){
        $(By.xpath("//*[contains(text(), 'Про компанію')]")).scrollTo();
        $(By.xpath("//*[contains(text(), 'Наші переваги')]")).scrollTo();
        $(By.xpath("//*[contains(text(), 'В нашій компанії цінують:')]")).scrollTo();
        $(By.xpath("//*[contains(text(),'Наші співробітники говорять про роботу в Onix')]")).scrollTo();
        $(By.xpath("//*[contains(text(),'IT-конвейер')]")).scrollTo();
        $(By.xpath("//*[contains(text(),'В тебе залишились питання?')]")).scrollTo();
        return this;
    }

    public void checkAboutCompanyBlock() {
        $(By.xpath("//*[contains(text(), 'Про компанію')]")).scrollTo();
        $$(photos).filter(Condition.visible).shouldHaveSize(4);
    }

    public void checkOurAdvantagesBlock() {
        $(By.xpath("//*[contains(text(), 'Наші переваги')]")).scrollTo();
        $$(advantages).filter(Condition.visible).shouldHaveSize(6);
    }

    public void checkOurValuesBlock() {
        $(By.xpath("//*[contains(text(), 'В нашій компанії цінують:')]")).scrollTo();
        $$(values).filter(Condition.visible).shouldHaveSize(2);
    }

    public void checkTestimonialsBlock() {
        $(By.xpath("//*[contains(text(),'Наші співробітники говорять про роботу в Onix')]")).scrollTo();
        $$(testimonials).filter(Condition.visible).shouldHaveSize(3);
    }

    public void checkContactUsBlock() {
        $(By.xpath("//*[contains(text(),'IT-конвейер')]")).scrollTo();
        $(By.xpath("//*[contains(text(),'В тебе залишились питання?')]")).scrollTo();
        contactWithUsButton.shouldBe(Condition.visible);
    }

    public void checkScrollElementsAreLoaded() {
        checkAboutCompanyBlock();
        checkOurAdvantagesBlock();
        checkOurValuesBlock();
        checkTestimonialsBlock();
        checkContactUsBlock();
    }
}
