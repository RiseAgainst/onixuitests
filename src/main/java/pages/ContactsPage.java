package pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import models.Aspirant;
import org.openqa.selenium.By;
import org.testng.Assert;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import static com.codeborne.selenide.Selenide.*;

public class ContactsPage {
    private SelenideElement contactForm = $(By.xpath("//*[@class = 'contact-form large']"));
    private SelenideElement contactInfoBlock = $(By.className("requirements"));
    private SelenideElement map = $(By.id("map"));
    private SelenideElement inputNameField = $(By.xpath("//*[@name='cf-name']"));
    private SelenideElement inputSurnameField = $(By.xpath("//*[@name='cf-surname']"));
    private SelenideElement inputEmailField = $(By.xpath("//*[@name='cf-email']"));
    private SelenideElement inputPhoneNumberField = $(By.xpath("//*[@name='cf-phone']"));
    private SelenideElement inputMessageField = $(By.xpath("//*[@name='cf-message']"));
    private SelenideElement uploader = $(By.id("file"));
    private ElementsCollection errorMessagesRequiredFields = $$(By.xpath("//*[@class='wpcf7-not-valid-tip']"));
    private SelenideElement sendButton = $(By.xpath("//*[@value= 'Відправити']"));
    private SelenideElement errorMessageInCorrectEmail = $(By.xpath("//span[text()='Email введено некоректно.']"));
    private SelenideElement vacancyDropDown = $(By.xpath("//*[@class='CSP-label CSP-toggle-dropdown CSP-placehoder']"));
    private ElementsCollection vacanciesListFromDropDown = $$(By.tagName("option"));
    // Expected data
    private static String expectedTitle = "Контакти – Onix-Systems";

    public ContactsPage(){ open("contacts");}

    public void checkTitle(){  Assert.assertTrue(title().equals(expectedTitle), "Wrong page title");}

    public void checkContactFormIsVisible() {
        contactForm.shouldBe(Condition.visible);
    }

    public void checkContactInfoFormIsVisible() {
        contactInfoBlock.shouldBe(Condition.visible);
        map.shouldBe(Condition.visible);
    }

    public void checkPageIsLoaded() {
        checkTitle();
        checkContactFormIsVisible();
        checkContactInfoFormIsVisible();
    }

    public void fillContactFormFields(Aspirant aspirant){
        fillFirstName(aspirant.getName());
        fillSurname(aspirant.getSurname());
        fillEmailField(aspirant.getEmail());
        fillPhone(aspirant.getPhone());
        fillMessage(aspirant.getMessage());
    }

    public void fillEmailField(String emailValue) {
        inputEmailField.val(emailValue);
    }

    public void fillFirstName(String nameValue) {
        inputNameField.val(nameValue);
    }

    public void fillSurname(String surnameValue) {
        inputSurnameField.val(surnameValue);
    }

    public void fillPhone(String phoneValue) {
        inputPhoneNumberField.val(phoneValue);
    }

    public void fillMessage(String messageValue) {
        inputMessageField.val(messageValue);
    }

    public List<String> getVacanciesFromDropDown() {
        List<String> temp = new ArrayList<>();
        for(SelenideElement el: vacanciesListFromDropDown) {
            temp.add(el.getValue());
        }
        return temp;
    }

    public void selectVacancy(String vacancyValue) {
        vacancyDropDown.click();
        $(By.xpath("//*[@data-value='" + vacancyValue + "']")).click();
    }

    public void clickSendButton(){sendButton.click();}

    public void checkErrorMessageIncorrectEmail(){
        errorMessageInCorrectEmail.shouldBe(Condition.visible);
    }

    public void checkErrorMessagesRequiredFields(){
        errorMessagesRequiredFields.shouldHaveSize(4);
    }

    public void uploadDefaultCv() {
        String separator = System.getProperty("file.separator");
        File file = new File(System.getProperty("user.dir") + separator + "src" + separator + "test" + separator + "resources" +  separator + "testfiles" + separator  + "cv.docx");
        uploader.uploadFile(file);
    }

    public void checkVacancyDropdownValues(List<String> expectedVacanciesList) {
        Assert.assertEquals(getVacanciesFromDropDown(), expectedVacanciesList, "Vacancies don't match");
    }

}
