package pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import org.testng.Assert;
import pages.vacancies.*;

import java.util.List;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.title;

public class VacanciesPage {
    private SelenideElement weOfferBlock = $(By.className("requirements"));
    private SelenideElement webDesignerVacanciesLink = $(By.xpath("//*[@href='https://onix.kr.ua/vacancies/web-ui-ux-dyzajner-dlya-stvorennya-sajtiv-ta-mobilnyh-dodatkiv/' and text() = 'Детальнiше']"));
    private SelenideElement projectManagerVacanciesLink = $(By.xpath("//*[@href='https://onix.kr.ua/vacancies/project-manager/' and text() = 'Детальнiше']"));
    private SelenideElement salesExecutiveLink = $(By.xpath("//*[@href='https://onix.kr.ua/vacancies/sales-executive/' and text() = 'Детальнiше']"));
    private SelenideElement unitySoftwareEngineer = $(By.xpath("//*[text() = 'Unity Software Engineer']"));
    private SelenideElement seniorIOSDeveloper = $(By.xpath("//*[text()='Senior iOS developer']"));
    private SelenideElement seniorPhpDeveloper = $(By.xpath("//*[text()='Senior PHP developer']"));
    private ElementsCollection vacanciesList = $$(By.xpath("//*[@rel='bookmark']"));
    private SelenideElement contactForm = $(By.xpath("//*[contains(text(),'Контактна форма')]"));

    // Expected data
    private static String expectedTitle = "Вакансії – Onix-Systems";

    public VacanciesPage(){ Selenide.open("vacancies");}

    public void checkContactInfoFormIsVisible() {
        weOfferBlock.shouldBe(Condition.visible);
    }

    public void checkTitle(){  Assert.assertTrue(title().equals(expectedTitle), "Wrong page title");}

    public void checkPageIsLoaded() {
        checkTitle();
        checkContactInfoFormIsVisible();
    }

    public List<String> getVacanciesListFromVacanciesPage(){
        List<String> temp;
        temp = vacanciesList.texts();
        return temp;
    }

    public WebDesignerPage clickWebDesignerLink(){
        webDesignerVacanciesLink.click();
        return new WebDesignerPage();
    }

    public ProjectManagerPage clickProjectManagerLink(){
        projectManagerVacanciesLink.click();
        return new ProjectManagerPage();
    }

    public SalesExecutivePage clickSalesExecutiveLink(){
        salesExecutiveLink.click();
        return new SalesExecutivePage();
    }

    public UnitySoftwareEngineerPage clickUnitySoftwareEngineer(){
        unitySoftwareEngineer.click();
        return new UnitySoftwareEngineerPage();
    }

    public SeniorIOSDeveloperPage clickSeniorIOSDeveloper(){
        seniorIOSDeveloper.click();
        return new SeniorIOSDeveloperPage();
    }

    public SeniorPHPDeveloperPage clickSeniorPhpDeveloper(){
        seniorPhpDeveloper.click();
        return new SeniorPHPDeveloperPage();
    }

}
