package utils;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;


import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public class Misc {

    public static Boolean isVisibleInViewport(SelenideElement element) {
     return (Boolean)((JavascriptExecutor)getWebDriver()).executeScript(
      "var el = arguments[0];                 " +
              "  var top = el.offsetTop;    " +
              "  var left = el.offsetLeft;         " +
              "  var width = el.offsetWidth;         " +
              "  var height = el.offsetHeight;" +
              "  while(el.offsetParent) {" +
              "el = el.offsetParent;" +
              "top += el.offsetTop;" +
              "left += el.offsetLeft; }" +
              "return (" +
                "top >= window.pageYOffset &&" +
                        "left >= window.pageXOffset &&" +
                        "(top + height) <= (window.pageYOffset + window.innerHeight) &&" +
                        "(left + width) <= (window.pageXOffset + window.innerWidth)" +
        ");", element);

}

    // Use when proper wait doesn't help
    public static void waitFor(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static boolean waitForJSandJQueryToLoad() {
        WebDriverWait wait = new WebDriverWait(getWebDriver(), 40);
        // wait for jQuery to load
        ExpectedCondition<Boolean> jQueryLoad = new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                try {
                    return ((Long)((JavascriptExecutor)driver).executeScript("return jQuery.active") == 0);
                }
                catch (Exception e) {
                    // no jQuery present
                    return true;
                }
            }
        };

        // wait for Javascript to load
        ExpectedCondition<Boolean> jsLoad = new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                return ((JavascriptExecutor)driver).executeScript("return document.readyState")
                        .toString().equals("complete");
            }
        };

        return wait.until(jQueryLoad) && wait.until(jsLoad);
    }

}
