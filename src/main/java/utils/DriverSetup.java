package utils;

import com.codeborne.selenide.Configuration;
import io.github.bonigarcia.wdm.ChromeDriverManager;

public class DriverSetup {
    private static String baseUrl = "https://onix.kr.ua/";

    public static void setupDriver(){
        Configuration.browser = "chrome";
        Configuration.timeout = 8000;
        Configuration.fastSetValue = false;
        Configuration.holdBrowserOpen = false;
        Configuration.baseUrl = baseUrl ;
        ChromeDriverManager.getInstance().setup();
    }
}
