package models;
import lombok.Data;

@Data
public class Aspirant {
    private String name;
    private String surname;
    private String email;
    private String phone;
    private String message;

    public Aspirant(String name, String surname, String email, String  phone, String message){
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.phone = phone;
        this.message = message;
    }

}
